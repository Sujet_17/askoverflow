AskOverFlow - README

-------------------------------------------------------------------------------
--------------------------------- Description ---------------------------------
-------------------------------------------------------------------------------

StackOverFlow like : site de question-réponse

Web application avec grails 2.4.4


-------------------------------------------------------------------------------
---------------------------- Lancement du programme ---------------------------
-------------------------------------------------------------------------------

Pour lancer le programme, il faut :

    - Se placer dans le répertoire du projet avec un invité de commande
    - Faire grails run-app

L'application doit se lancer sur : http://localhost:8080/AskOverFlow/



-------------------------------------------------------------------------------
-------------------------------- Documentation --------------------------------
-------------------------------------------------------------------------------

La documentation liée au site se trouve dans le dossier /doc à la racine du
projet.

Il y a un diagramme UML, les spécifications et des mockups réalisés pour créer
l'interface.


-------------------------------------------------------------------------------
-------------------------------- Liste des bugs -------------------------------
-------------------------------------------------------------------------------

- Utilisateur reste connecté quand il supprime son compte.
- On peut ajouter plusieurs objets en même temps si on appuie très vite sur le
bouton

