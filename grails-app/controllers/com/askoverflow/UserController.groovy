package com.askoverflow



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UserController
{
    static Boolean linkMe = true;
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max)
    {
        respond User.list(sort: "reputation", order: "desc"), model:[userInstanceCount: User.count()]
    }

    def show(User userInstance) {
        respond userInstance
    }

    def create() {
        respond new User(params)
    }

    @Transactional
    def save(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'create'
            return
        }

        userInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'user.created.message', default: 'User created !')
                redirect userInstance
            }
            '*' { respond userInstance, [status: CREATED] }
        }
    }

    def edit(User userInstance)
    {
        respond userInstance;
    }

    @Transactional
    def update(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }

        // security verification - same user
        if (userInstance.id.toString() == sec.loggedInUserInfo(field: 'id').toString())
        {
            if (userInstance.hasErrors()) {
                respond userInstance.errors, view:'edit'
                return
            }

            userInstance.save flush:true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'user.updated.message', default: 'User updated !')
                    redirect userInstance
                }
                '*'{ respond userInstance, [status: OK] }
            }
        }
    }

    def deleteUserService;
    def userRoleService;

    @Transactional
    def delete(User userInstance) {

        if (userInstance == null) {
            notFound()
            return
        }

        // security verification - same user
        if (userInstance.id.toString() == sec.loggedInUserInfo(field: 'id').toString())
        {
            // last admin association delete control and Unknown user control
            Boolean canBeDeleted = userRoleService.userCanBeDeleted(userInstance);
            Boolean isUnknownUser = deleteUserService.userIsUnknownUser(userInstance);
            if (! isUnknownUser && canBeDeleted)
            {
                // update user's post, votes and roles
                deleteUserService.updateUserContentOnDelete(userInstance);

                userInstance.delete flush:true

                request.withFormat {
                    form multipartForm {
                        flash.message = message(code: 'user.deleted.message', default: 'User deleted !')
                        redirect action:"index", method:"GET"
                    }
                    '*'{ render status: NO_CONTENT }
                }
            }
            else
            {
                request.withFormat {
                    form multipartForm {
                        if (isUnknownUser)
                        {
                            flash.message = message(code: 'user.unknown.cant.be.deleted.message', default: 'User Unknown can not be deleted !');
                        }
                        else
                        {
                            flash.message = message(code: 'userRole.cant.be.deleted.message', default: 'UserRole can not be deleted !');    
                        }
                        redirect userInstance
                    }
                    '*'{ render status: NO_CONTENT }
                }
            }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
