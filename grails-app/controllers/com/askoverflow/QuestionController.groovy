package com.askoverflow



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class QuestionController
{
    static Boolean linkMe = true;
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", downVote: "POST", upVote: "POST"]

    def index(Integer max)
    {
        respond Question.list(sort: "vote", order: "desc"), model:[questionInstanceCount: Question.count()]
    }

    def show(Question questionInstance) {
        respond questionInstance
    }

    def create() {
        respond new Question(params)
    }

    def badgeService;

    @Transactional
    def save(Question questionInstance) {
        if (questionInstance == null) {
            notFound()
            return
        }

        if (questionInstance.hasErrors()) {
            respond questionInstance.errors, view:'create'
            return
        }

        questionInstance.save flush:true

        // badge check for the author
        badgeService.checkForBadges(questionInstance.author);

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'question.created.message', default: 'Question created !')
                redirect questionInstance
            }
            '*' { respond questionInstance, [status: CREATED] }
        }
    }

    def edit(Question questionInstance)
    {
        render(template: "update", bean: questionInstance, var:"questionInstance");
    }

    @Transactional
    def update(Question questionInstance) {
        if (questionInstance == null) {
            notFound()
            return
        }

        if (questionInstance.hasErrors()) {
            respond questionInstance.errors, view:'edit'
            return
        }

        questionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'question.updated.message', default: 'Question updated !')
                redirect questionInstance
            }
            '*'{ respond questionInstance, [status: OK] }
        }
    }

    def deleteQuestionService;

    @Transactional
    def delete(Question questionInstance) {

        if (questionInstance == null) {
            notFound()
            return
        }

        // delete vote associations ans update author's reputation
        deleteQuestionService.updateVoteAndAuthorOnQuestionDelete(questionInstance);

        questionInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'question.deleted.message', default: 'Question deleted !')
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def downVoteService;
    def upVoteService;

    @Transactional
    def upVote(Question questionInstance)
    {
        // security verification - not same user
        String loggedUserId = sec.loggedInUserInfo(field: 'id').toString();
        if (questionInstance.author.id.toString() != loggedUserId)
        {
            upVoteService.upVoteQuestion(questionInstance, Long.parseLong(loggedUserId));
            render questionInstance.vote;
        }
    }

    @Transactional
    def downVote(Question questionInstance)
    {
        // security verification - not same user
        String loggedUserId = sec.loggedInUserInfo(field: 'id').toString();
        if (questionInstance.author.id.toString() != loggedUserId)
        {
            downVoteService.downVoteQuestion(questionInstance, Long.parseLong(loggedUserId));
            render questionInstance.vote;
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'question.label', default: 'Question'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
