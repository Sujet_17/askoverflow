package com.askoverflow



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TagController
{
    static Boolean linkMe = true;
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max)
    {
        respond Tag.list(), model:[tagInstanceCount: Tag.count()]
    }

    def show(Tag tagInstance) {
        respond tagInstance
    }

    def create() {
        respond new Tag(params)
    }

    @Transactional
    def save(Tag tagInstance) {
        if (tagInstance == null) {
            notFound()
            return
        }

        if (tagInstance.hasErrors()) {
            respond tagInstance.errors, view:'create'
            return
        }

        tagInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'tag.created.message', default: 'Tag created !')
                redirect tagInstance
            }
            '*' { respond tagInstance, [status: CREATED] }
        }
    }

    def edit(Tag tagInstance)
    {
        render(template: "update", bean: tagInstance, var:"tagInstance");
    }

    @Transactional
    def update(Tag tagInstance) {
        if (tagInstance == null) {
            notFound()
            return
        }

        if (tagInstance.hasErrors()) {
            respond tagInstance.errors, view:'edit'
            return
        }

        tagInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'tag.updated.message', default: 'Tag updated !')
                redirect tagInstance
            }
            '*'{ respond tagInstance, [status: OK] }
        }
    }

    def deleteTagService;

    @Transactional
    def delete(Tag tagInstance) {

        if (tagInstance == null) {
            notFound()
            return
        }

        // check that there is no question linked with the tag
        if (deleteTagService.tagHasNoQuestion(tagInstance))
        {
            tagInstance.delete flush:true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'tag.deleted.message', default: 'Tag deleted !')
                    redirect action:"index", method:"GET"
                }
                '*'{ render status: NO_CONTENT }
            }
        }
        else
        {
            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'tag.cant.be.deleted.message', default: 'Tag cannot be deleted !')
                    redirect tagInstance
                }
                '*'{ render status: NO_CONTENT }
            }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tag.label', default: 'Tag'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
