package com.askoverflow



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AnswerController
{
    static Boolean linkMe = true;
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", downVote: "POST", upVote: "POST"]

    def index(Integer max)
    {
        respond Answer.list(sort: "vote", order: "desc"), model:[answerInstanceCount: Answer.count()]
    }

    def show(Answer answerInstance) {
        respond answerInstance
    }

    def create()
    {
        def newAnswer = new Answer(params);
        render(template: "new", bean: newAnswer, var:"answerInstance");
    }

    def badgeService;

    @Transactional
    def save(Answer answerInstance) {
        if (answerInstance == null) {
            notFound()
            return
        }

        if (answerInstance.hasErrors()) {
            respond answerInstance.errors, view:'create'
            return
        }

        answerInstance.save flush:true

        // badge check for the author
        badgeService.checkForBadges(answerInstance.author);

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'answer.created.message', default: 'Answer created')
                redirect answerInstance.question
            }
            '*' { respond answerInstance, [status: CREATED] }
        }
    }

    def edit(Answer answerInstance)
    {
        render(template: "update", bean: answerInstance, var:"answerInstance");
    }

    @Transactional
    def update(Answer answerInstance) {
        if (answerInstance == null) {
            notFound()
            return
        }

        if (answerInstance.hasErrors()) {
            respond answerInstance.errors, view:'edit'
            return
        }

        answerInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'answer.updated.message', default: 'Answer updated !')
                redirect answerInstance.question
            }
            '*'{ respond answerInstance, [status: OK] }
        }
    }

    def deleteAnswerService;

    @Transactional
    def delete(Answer answerInstance) {

        if (answerInstance == null) {
            notFound()
            return
        }

        // delete vote associations ans update author's reputation
        deleteAnswerService.updateVoteAndAuthorOnAnswerDelete(answerInstance);

        // question recuperation for the redirection
        Question redirectQuestion = answerInstance.question;

        answerInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'anwser.deleted.message', default: 'Answer deleted !')
                redirect redirectQuestion
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def downVoteService;
    def upVoteService;

    @Transactional
    def upVote(Answer answerInstance)
    {
        // security verification - not same user
        String loggedUserId = sec.loggedInUserInfo(field: 'id').toString();
        if (answerInstance.author.id.toString() != loggedUserId)
        {
            upVoteService.upVoteAnswer(answerInstance, Long.parseLong(loggedUserId));
            render answerInstance.vote;
        }
    }

    @Transactional
    def downVote(Answer answerInstance)
    {
        // security verification - not same user
        String loggedUserId = sec.loggedInUserInfo(field: 'id').toString();
        if (answerInstance.author.id.toString() != loggedUserId)
        {
            downVoteService.downVoteAnswer(answerInstance, Long.parseLong(loggedUserId));
            render answerInstance.vote;
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'answer.label', default: 'Answer'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
