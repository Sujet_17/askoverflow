package com.askoverflow



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class BadgeController
{
    static Boolean linkMe = false;
    static allowedMethods = [list: "GET"]

    def index(Integer max)
    {
        respond Badge.list(), model:[badgeInstanceCount: Badge.count()]
    }
}
