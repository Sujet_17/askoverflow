package com.askoverflow


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UserRoleController
{
    static Boolean linkMe = true;
    static allowedMethods = [save: "POST", delete: "DELETE"]

    def index(Integer max)
    {
        respond UserRole.list(), model:[userRoleInstanceCount: UserRole.count()]
    }

    def create() {
        respond new UserRole(params)
    }

    def badgeService;

    @Transactional
    def save(UserRole userRoleInstance) {

        if (userRoleInstance == null) {
            notFound()
            return
        }

        if (userRoleInstance.hasErrors()) {
            respond userRoleInstance.errors, view:'create'
            return
        }

        UserRole.create(userRoleInstance.user, userRoleInstance.role, true);

        // badge check for the concerned user
        badgeService.checkForBadges(userRoleInstance.user);

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'userRole.created.message', default: 'UserRole created !')
                redirect action: "index", method: "GET"
            }
            '*' { render status: CREATED }
        }
    }

    def userRoleService;

    @Transactional
    def delete(Long userId, Long roleId) {

        if (! UserRole.exists(userId, roleId)) {
            notFound()
            return
        }

        // last admin association delete control
        Role role = Role.findById(roleId);
        if (userRoleService.roleAssociationCanBeDeleted(role))
        {
            User concernedUser = User.findById(userId);
            UserRole.remove(concernedUser, role, true);

            // badge check for the concerned user
            badgeService.checkForBadges(concernedUser);

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'userRole.deleted.message', default: 'UserRole deleted !')
                    redirect action:"index", method:"GET"
                }
                '*'{ render status: NO_CONTENT }
            }
        }
        else
        {
            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'userRole.cant.be.deleted.message', default: 'UserRole can not be deleted !')
                    redirect action:"index", method:"GET"
                }
                '*'{ render status: NO_CONTENT }
            }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'userRole.label', default: 'UserRole'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
