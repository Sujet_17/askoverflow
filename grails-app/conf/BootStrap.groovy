import com.askoverflow.Role
import com.askoverflow.User
import com.askoverflow.UserRole
import com.askoverflow.Badge

class BootStrap {

    def init = { servletContext ->

        // truncateToString()
        Object.metaClass.truncateToString = {Integer stringLength ->
 
            String trimString = delegate?.toString()
            String concatenateString = "..."
            List separators = [".", " "]
         
            if (stringLength && (trimString?.length() > stringLength)) {
                trimString = trimString.substring(0, stringLength - concatenateString.length())
                String separator = separators.findAll{trimString.contains(it)}?.min{trimString.lastIndexOf(it)}
                if(separator){
                    trimString = trimString.substring(0, trimString.lastIndexOf(separator))
                }
                trimString += concatenateString
            }
            return trimString
        }

        // default roles
        def adminRole = new Role(authority: 'ROLE_ADMIN', name: 'Administrator').save(flush: true)
        def moderatorRole = new Role(authority: 'ROLE_MODERATOR', name: 'Moderator').save(flush: true)

        // admin user
        def adminUser = new User(username: 'Admin', enabled: true, password: 'admin', email: 'admin@askoverflow.com')
        adminUser.save(flush: true)
        UserRole.create adminUser, adminRole, true

        // unknown user
        def unknownUser = new User(username: 'Unknown', enabled: true, password: 'unknown', email: 'unknown@askoverflow.com')
        unknownUser.save(flush: true)

        // badges
        /* --------------------------------------
                  Badge pattern convention

        // 0
        def badge0 = new Badge(switchNumber: 0, name: "Badge name", description: "Badge description");
        badge0.save(flush: true);

        ---------------------------------------- */

        // 0
        def badge0 = new Badge(switchNumber: 0, name: "Newbie", description: "Ask one question");
        badge0.save(flush: true);

        // 1
        def badge1 = new Badge(switchNumber: 1, name: "Up and down", description: "Vote 10 times");
        badge1.save(flush: true);

        // 2
        def badge2 = new Badge(switchNumber: 2, name: "Padawan", description: "Became Yoda's padawan with at least 10 in reputation");
        badge2.save(flush: true);

        // 3
        def badge3 = new Badge(switchNumber: 3, name: "Jedi master", description: "Became a Jedi master with at least 250 in reputation");
        badge3.save(flush: true);

        // 4
        def badge4 = new Badge(switchNumber: 4, name: "Knowledge owner or not", description: "Post 20 answers");
        badge4.save(flush: true);

        // 5
        def badge5 = new Badge(switchNumber: 5, name: "Apprentice", description: "Became an apprentice with at least -10 in reputation");
        badge5.save(flush: true);

        // 6
        def badge6 = new Badge(switchNumber: 6, name: "Sith emperor", description: "Became a Sith emperor with at least -100 in reputation");
        badge6.save(flush: true);

    }

    def destroy = {
    }
}
