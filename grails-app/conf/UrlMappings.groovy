class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller:'Question')
        "500"(view:'/error')
        "404"(view:'/404')
        "/admin"(view:"/index")
	}
}
