<%@ page import="com.askoverflow.Role" %>

<h1><g:message code="role.edit.label" default="Edit the role" /></h1>
            
<g:hasErrors bean="${roleInstance}">
<ul class="errors" role="alert">
    <g:eachError bean="${roleInstance}" var="error">
    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
    </g:eachError>
</ul>
</g:hasErrors>
<g:form url="[resource:roleInstance, action:'update']" method="PUT" >
    <g:hiddenField name="version" value="${roleInstance?.version}" />
    <fieldset class="form">
        <g:render template="form"/>
    </fieldset>
    <fieldset class="buttons">
        <g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
    </fieldset>
</g:form>
