
<%@ page import="com.askoverflow.Role" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}" />
		<title><g:message code="role.show.label" default="Show role" /> : <g:fieldValue bean="${roleInstance}" field="name"/> - Ask Over Flow</title>
	</head>
	<body>
		<a href="#show-role" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link elementId="questionButton" class="list" controller="question" action="index"><g:message code="question.list.label" default="Question list" /></g:link></li>

				<li><g:link elementId="tagButton" class="list" controller="tag" action="index"><g:message code="tag.list.label" default="Tag list" /></g:link></li>

				<li><g:link elementId="badgeButton" class="list" controller="badge" action="index"><g:message code="badge.list.label" default="Badge list" /></g:link></li>

				<li><g:link elementId="userButton" class="list" controller="user" action="index"><g:message code="user.list.label" default="User list" /></g:link></li>

				<sec:ifAllGranted roles="ROLE_ADMIN">
					<li><g:link elementId="roleButton" class="list" action="index"><g:message code="role.list.label" default="Role list" /></g:link></li>

					<li><g:link elementId="userRoleButton" class="list" controller="userRole" action="index"><g:message code="userRole.list.label" default="UserRole list" /></g:link></li>
				</sec:ifAllGranted>

				<sec:ifLoggedIn>
					<li><g:link elementId="profileButton" class="list" controller="user" action="show" id="${sec.loggedInUserInfo(field: 'id')}"><sec:username/></g:link></li>
				</sec:ifLoggedIn>
			</ul>
		</div>
		<div id="show-role" class="content scaffold-show" role="main">
		
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

			<div id="role_${roleInstance.id}">

				<h1><g:message code="role.show.label" default="Show role" /></h1>

				<table class="role">
		        <tbody>
		        <tr class="role-tr">
		            <td class="rolecell">    
						<div class="role">

							<span id="description-label" class="property-label"><g:message code="role.name.label" default="Name" /></span>
						
							<span class="property-value" aria-labelledby="description-label"><div><g:fieldValue bean="${roleInstance}" field="name"/></div></span>

						</div>
		            </td>
		            
					<td class="infocell">

						<table class="role">
						<tbody>
						<tr class="role-tr">
							<td class="listcell">
								<div class="listhead">

									<span id="questions-count" class="property-label">${roleInstance?.users.size()}</span>

									<span id="questions-label" class="property-label"><g:message code="role.users.label" default="Users" /></span>

								</div>
							</td>
						</tr>
						<tr class="role-tr">
							<td class="listcell">
								<table class="role">
								<tbody>
								<g:each in="${roleInstance.users}" var="u">
									<tr class="role-tr">

										<td class="listvotecell"><span class="property-value" aria-labelledby="user-label"><g:fieldValue bean="${u}" field="reputation"/></span>
										</td>

										<td class="listtextcell"><span class="property-value" aria-labelledby="user-label"><g:link controller="user" action="show" id="${u.id}">${u?.encodeAsHTML()}</g:link></span>
										</td>
									</tr>
								</g:each>
								</tbody>
								</table>
							</td>
						</tr>
						</tbody>
						</table>
					</td>
				</tr>
				</tbody>
				</table>

				<g:form url="[resource:roleInstance, action:'delete']" method="DELETE">
					<fieldset class="buttons">
						<sec:ifAllGranted roles="ROLE_ADMIN">
							<g:remoteLink class="edit" action="edit" resource="${roleInstance}" update="[success: "role_${roleInstance.id}"]"><g:message code="default.button.edit.label" default="Edit" /></g:remoteLink>
						</sec:ifAllGranted>
					</fieldset>
				</g:form>
			</div>
		</div>
	</body>
</html>
