<%@ page import="com.askoverflow.Role" %>


<div class="fieldcontain ${hasErrors(bean: roleInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="role.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" maxlength="15" required="" value="${roleInstance?.name}"/>

</div>

<g:hiddenField id="authority" name="authority" value="${roleInstance?.authority}" />
