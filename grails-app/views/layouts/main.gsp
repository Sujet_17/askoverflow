<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Ask Over Flow"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
  		<asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>
		<g:layoutHead/>
	</head>
	<body>

		<div id="askoverflowlogo" role="banner">
			<a class="home" href="${createLink(uri: '/')}"><asset:image src="askoverflow_ban.png" alt="AskOverFlow"/></a>
			
			<sec:ifNotLoggedIn>
				<g:link elementId="signInButton" class="styleButton" controller="user" action="create"><g:message code="user.new.label" default="New user" /></g:link>

				<g:link elementId="logInButton" class="styleButton" controller="login" action="auth"><g:message code="user.login.label" default="Log in" /></g:link>
		 	</sec:ifNotLoggedIn>

		 	<sec:ifLoggedIn>
				<g:remoteLink elementId="logOutButton" class="styleButton" controller="logout" method="post" asynchronous="false" onSuccess="location.reload()"><g:message code="user.logout.label" default="Log out" /></g:remoteLink>
			</sec:ifLoggedIn>
		</div>

		<g:layoutBody/>
		<div class="footer" role="contentinfo">
			<span class="property-value">&copy;2015 - Askoverflow - Johan Maupetit et Camille LLamas - F2 - Promotion 2015 - ISIMA</span>
			<sec:ifAllGranted roles="ROLE_ADMIN">
				<g:link elementId="buttonAdmin" class="styleButton" controller="admin"><g:message code="button.admin.label" default="Administrator" /></g:link>
			</sec:ifAllGranted>		
		</div>
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
	</body>
</html>
