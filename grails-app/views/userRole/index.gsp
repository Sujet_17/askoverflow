
<%@ page import="com.askoverflow.UserRole" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main">
        <g:set var="entityName" value="${message(code: 'userRole.label', default: 'UserRole')}" />
        <title><g:message code="userRole.list.label" default="UserRole list" /> - Ask Over Flow</title>
    </head>
    <body>
        <a href="#list-userRole" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><g:link elementId="questionButton" class="list" controller="question" action="index"><g:message code="question.list.label" default="Question list" /></g:link></li>
                
                <li><g:link elementId="tagButton" class="list" controller="tag" action="index"><g:message code="tag.list.label" default="Tag list" /></g:link></li>

                <li><g:link elementId="badgeButton" class="list" controller="badge" action="index"><g:message code="badge.list.label" default="Badge list" /></g:link></li>

                <li><g:link elementId="userButton" class="list" controller="user" action="index"><g:message code="user.list.label" default="User list" /></g:link></li>

                <sec:ifAllGranted roles="ROLE_ADMIN">
                    <li><g:link elementId="roleButton" class="list" controller="role" action="index"><g:message code="role.list.label" default="Role list" /></g:link></li>

                    <li><g:link class="create" action="create"><g:message code="userRole.new.label" default="New authorization" /></g:link></li>
                </sec:ifAllGranted>

                <sec:ifLoggedIn>
                    <li><g:link elementId="profileButton" class="list" controller="user" action="show" id="${sec.loggedInUserInfo(field: 'id')}"><sec:username/></g:link></li>
                </sec:ifLoggedIn>
            </ul>
        </div>
        <div id="list-userRole" class="content scaffold-list" role="main">
            <h1><g:message code="userRole.list.label" default="UserRole list" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>

            <table>
            <thead>
                    <tr>
                    
                        <th><g:message code="userRole.user.label" default="User" /></th>

                        <th><g:message code="userRole.role.label" default="Role" /></th>

                        <th><g:message code="userRole.action.label" default="Actions" /></th>
                    
                    </tr>
                </thead>
                <tbody>
                <g:each in="${userRoleInstanceList}" status="i" var="userRoleInstance">
                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                    
                        <td>${fieldValue(bean: userRoleInstance, field: "user")}</td>
                    
                        <td>${fieldValue(bean: userRoleInstance, field: "role")}</td>

                        <td>
                            <g:form url="[action:'delete']" method="DELETE">

                                <g:hiddenField name="userId" value="${userRoleInstance.user.id}" />
                                <g:hiddenField name="roleId" value="${userRoleInstance.role.id}" />

                                <fieldset class="buttons">

                                <sec:ifAllGranted roles="ROLE_ADMIN">
                                    <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                                </sec:ifAllGranted>
                            </fieldset>
                        </g:form>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
            <div class="pagination">
                <g:paginate total="${userInstanceCount ?: 0}" />
            </div>
        </div>
    </body>
</html>
