<%@ page import="com.askoverflow.UserRole" %>


<div class="fieldcontain ${hasErrors(bean: userRoleInstance, field: 'user', 'error')} required">
    <label for="user">
        <g:message code="userRole.user.label" default="User" />
        <span class="required-indicator">*</span>
    </label>
    <g:select name="user" required="" from="${com.askoverflow.User.list()}" optionKey="id" value="${userRoleInstance?.user?.id}" />

</div>

<div class="fieldcontain ${hasErrors(bean: userRoleInstance, field: 'role', 'error')} required">
    <label for="role">
        <g:message code="userRole.role.label" default="Role" />
        <span class="required-indicator">*</span>
    </label>
    <g:select name="role" required="" from="${com.askoverflow.Role.list()}" optionKey="id" value="${userRoleInstance?.role?.id}" />

</div>
