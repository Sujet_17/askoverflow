<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main">
        <g:set var="entityName" value="${message(code: 'userRole.label', default: 'UserRole')}" />
        <title><g:message code="userRole.create.label" default="Authorization creation" /> - Ask Over Flow</title>
    </head>
    <body>
        <a href="#create-userRole" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><g:link elementId="questionButton" class="list" controller="question" action="index"><g:message code="question.list.label" default="Question list" /></g:link></li>
                
                <li><g:link elementId="tagButton" class="list" controller="tag" action="index"><g:message code="tag.list.label" default="Tag list" /></g:link></li>

                <li><g:link elementId="badgeButton" class="list" controller="badge" action="index"><g:message code="badge.list.label" default="Badge list" /></g:link></li>

                <li><g:link elementId="userButton" class="list" controller="user" action="index"><g:message code="user.list.label" default="User list" /></g:link></li>

                <sec:ifAllGranted roles="ROLE_ADMIN">
                    <li><g:link elementId="roleButton" class="list" controller="role" action="index"><g:message code="role.list.label" default="Role list" /></g:link></li>

                    <li><g:link elementId="userRoleButton" class="list" action="index"><g:message code="userRole.list.label" default="Authorization list" /></g:link></li>
                </sec:ifAllGranted>

                <sec:ifLoggedIn>
                    <li><g:link elementId="profileButton" class="list" controller="user" action="show" id="${sec.loggedInUserInfo(field: 'id')}"><sec:username/></g:link></li>
                </sec:ifLoggedIn>
            </ul>
        </div>
        <div id="create-userRole" class="content scaffold-create" role="main">
            
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>

            <h1><g:message code="userRole.create.label" default="Authorization creation" /></h1>
            
            <g:hasErrors bean="${userRoleInstance}">
            <ul class="errors" role="alert">
                <g:eachError bean="${userRoleInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message code="userRole.not.unique.error"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form url="[resource:userRoleInstance, action:'save']" >
                <fieldset class="form">
                    <g:render template="form"/>
                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
