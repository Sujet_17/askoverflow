
<%@ page import="com.askoverflow.User" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
		<title><g:message code="user.list.label" default="User list" /> - Ask Over Flow</title>
	</head>
	<body>
		<a href="#list-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link elementId="questionButton" class="list" controller="question" action="index"><g:message code="question.list.label" default="Question list" /></g:link></li>

                <li><g:link elementId="badgeButton" class="list" controller="badge" action="index"><g:message code="badge.list.label" default="Badge list" /></g:link></li>
                
                <li><g:link elementId="tagButton" class="list" controller="tag" action="index"><g:message code="tag.list.label" default="Tag list" /></g:link></li>

                <li><g:link class="create" controller="question" action="create"><g:message code="question.new.label" default="Ask a question" /></g:link></li>

                <sec:ifLoggedIn>
                    <li><g:link elementId="profileButton" class="list" controller="user" action="show" id="${sec.loggedInUserInfo(field: 'id')}"><sec:username/></g:link></li>
                </sec:ifLoggedIn>
			</ul>
		</div>
		<div id="list-user" class="content scaffold-list" role="main">
			<h1><g:message code="user.list.label" default="User list" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <table>
            <thead>
                    <tr>
                    
                        <th><g:message code="user.username.label" default="Username" /></th>

                        <th><g:message code="user.reputation.label" default="Reputation" /></th>

                        <th><g:message code="user.incriptionDate.label" default="Inscription Date" /></th>
                    
                    </tr>
                </thead>
                <tbody>
                <g:each in="${userInstanceList}" status="i" var="userInstance">
                    <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                    
                        <td><g:link action="show" id="${userInstance.id}">${fieldValue(bean: userInstance, field: "username")}</g:link></td>
                    
                        <td>${fieldValue(bean: userInstance, field: "reputation")}</td>

                        <td><g:formatDate date="${userInstance?.incriptionDate}" /></td>
                    
                    </tr>
                </g:each>
                </tbody>
            </table>
			<div class="pagination">
				<g:paginate total="${userInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
