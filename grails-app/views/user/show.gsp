
<%@ page import="com.askoverflow.User" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
		<title><g:fieldValue bean="${userInstance}" field="username"/> - <g:message code="user.show.label" default="Show the user" /> - Ask Over Flow</title>
	</head>
	<body>
		<a href="#show-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link elementId="questionButton" class="list" controller="question" action="index"><g:message code="question.list.label" default="Question list" /></g:link></li>

				<li><g:link elementId="tagButton" class="list" controller="tag" action="index"><g:message code="tag.list.label" default="Tag list" /></g:link></li>

				<li><g:link elementId="badgeButton" class="list" controller="badge" action="index"><g:message code="badge.list.label" default="Badge list" /></g:link></li>

				<li><g:link elementId="userButton" class="list" action="index"><g:message code="user.list.label" default="User list" /></g:link></li>

				<li><g:link class="create" controller="question" action="create"><g:message code="question.new.label" default="Ask a question" /></g:link></li>

				<sec:ifLoggedIn>
                    <li><g:link elementId="profileButton" class="list" controller="user" action="show" id="${sec.loggedInUserInfo(field: 'id')}"><sec:username/></g:link></li>
                </sec:ifLoggedIn>
			</ul>
		</div>
		<div id="show-user" class="content scaffold-show" role="main">

			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

			<div id="user_${userInstance.id}">

				<h1><span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${userInstance}" field="username"/></span></h1>

				<table class="profile">
		        <tbody>
		        <tr class="profile-tr">
		            <td class="reputationcell">    
						<div class="reputation">

							<span id="reputation-label" class="property-label"><g:message code="user.reputation.label" default="Reputation" /></span>
						
							<span class="property-value" aria-labelledby="reputation-label"><div><g:fieldValue bean="${userInstance}" field="reputation"/></div></span>

						</div>
		            </td>
		            
					<td class="infocell">

						<table class="profile">
						<tbody>
						<tr class="profile-tr">
							<td class="infolabel"><span id="email-label" class="property-label"><g:message code="user.email.label" default="Email" /> :</span>
							</td>
							<td class="infocell"><span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${userInstance}" field="email"/></span>
							</td>
						</tr>
						<tr class="profile-tr">
							<td class="infolabel"><span id="email-label" class="property-label"><g:message code="user.incriptionDate.label" default="Incription Date" /> :</span>
							</td>
							<td class="infocell"><span class="property-value" aria-labelledby="incriptionDate-label"><g:formatDate date="${userInstance?.incriptionDate}" /></span>
							</td>
						</tr>
						<tr class="profile-tr">
							<td class="infolabel"><span id="grades-label" class="property-label"><g:message code="user.grades.label" default="Grades" /> :</span>
							</td>
							<td class="infocell">
							<g:each in="${userInstance.getAuthorities()}" var="g">
								<span class="property-value" aria-labelledby="grades-label"><g:link controller="role" action="show" id="${g.id}">${g?.encodeAsHTML()}</g:link></span>
							</g:each>
							</td>
						</tr>
						</tbdoy>
						</table>
					</td>
				</tr>
				</tbody>
				</table>

				<table class="profile">
				<tbody>
				<tr class="profile-tr">
					<td class="listcell">
						<div class="listhead">
							<span id="questions-count" class="property-label">${userInstance?.questions.size()}</span>

							<span id="questions-label" class="property-label"><g:message code="user.questions.label" default="Questions" /></span>
						</div>
					</td>
					<td class="listcell">
						<div class="listhead">
							<span id="answers-count" class="property-label">${userInstance?.answers.size()}</span>

							<span id="answer-label" class="property-label"><g:message code="user.answers.label" default="Answers" /></span>
						</div>
					</td>
					<td class="listcell">
						<div class="listhead">
							<span id="earnedBadges-count" class="property-label">${userInstance?.getEarnedBadges().size()}</span>

							<span id="earnedBadges-label" class="property-label"><g:message code="user.earnedBadges.label" default="Earned badges" /></span>
						</div>
					</td>
				</tr>
				<tr class="profile-tr">
					<td class="tablecell">
						<table class="profile">
						<tbody>

						<g:each in="${userInstance.questions}" var="q">
							<tr class="profile-tr">

								<td class="listvotecell"><span class="property-value" aria-labelledby="questions-label"><g:fieldValue bean="${q}" field="vote"/></span>
								</td>

								<td class="listtextcell"><span class="property-value" aria-labelledby="questions-label"><g:link controller="question" action="show" id="${q.id}">${q?.truncateToString(30).encodeAsHTML()}</g:link></span>
								</td>
							</tr>
						</g:each>
						</tbody>
						</table>
					</td>
					<td class="tablecell">
						<table class="profile">
						<tbody>

						<g:each in="${userInstance.answers}" var="a">
							<tr class="profile-tr">

								<td class="listvotecell"><span class="property-value" aria-labelledby="answer-label"><g:fieldValue bean="${a}" field="vote"/></span>
								</td>

								<td class="listtextcell"><span class="property-value" aria-labelledby="answer-label"><g:link controller="question" action="show" id="${a.question.id}">${a?.truncateToString(30).encodeAsHTML()}</g:link></span>
								</td>
							</tr>
						</g:each>
						</tbody>
						</table>
					</td>
					<td class="tablecell">
						<table class="profile">
						<tbody>

						<g:each in="${userInstance.getEarnedBadges()}" var="b">
							<tr class="profile-tr">
								<td><span class="property-value" aria-labelledby="earnedBadge-label"><g:fieldValue bean="${b}" field="name"/></span></td>
							</tr>
						</g:each>
						</tbody>
						</table>
					</td>
				</tr>
				</tbody>
				</table>
				
				<g:form url="[resource:userInstance, action:'delete']" method="DELETE">
					<fieldset class="buttons">

						<g:if test="${userInstance.id.toString() == sec.loggedInUserInfo(field: 'id').toString()}">
							<g:link class="edit" action="edit" resource="${userInstance}"><g:message code="user.button.edit.label" default="Edit" /></g:link>

							<g:if test="${userInstance.username != 'Unknown'}">
								<g:actionSubmit class="delete" action="delete" value="${message(code: 'user.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</g:if>
						</g:if>
					</fieldset>
				</g:form>
			</div>
		</div>
	</body>
</html>
