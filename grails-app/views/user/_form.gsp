<%@ page import="com.askoverflow.User" %>


<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="user.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>

	<g:if test="${hasErrors(bean: userInstance, field: 'username', 'error')}">
		<g:textField name="username" maxlength="15" required="" value="${userInstance?.username}"/>
	</g:if>
	<g:elseif test="${userInstance.username != 'Unknown'}">
		<g:textField name="username" maxlength="15" required="" value="${userInstance?.username}"/>
	</g:elseif>
	<g:else>
		<span><g:fieldValue bean="${userInstance}" field="username"/></span>
		<g:hiddenField id="username" name="username" value="${userInstance?.username}" />
	</g:else>

</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="user.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="password" maxlength="15" required="" value=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="user.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="email" required="" value="${userInstance?.email}"/>

</div>
