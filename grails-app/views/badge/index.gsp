
<%@ page import="com.askoverflow.Badge" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'badge.label', default: 'Badge')}" />
		<title><g:message code="badge.list.label" args="Badge list" /></title>
	</head>
	<body>
		<a href="#list-badge" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link elementId="questionButton" class="list" controller="question" action="index"><g:message code="question.list.label" default="Question list" /></g:link></li>
				
				<li><g:link elementId="tagButton" class="list" controller="tag" action="index"><g:message code="tag.list.label" default="Tag list" /></g:link></li>
				
				<li><g:link elementId="userButton" class="list" controller="user" action="index"><g:message code="user.list.label" default="User list" /></g:link></li>

				<li><g:link class="create" controller="question" action="create"><g:message code="question.new.label" default="Ask a question" /></g:link></li>

				<sec:ifLoggedIn>
					<li><g:link elementId="profileButton" class="list" controller="user" action="show" id="${sec.loggedInUserInfo(field: 'id')}"><sec:username/></g:link></li>
				</sec:ifLoggedIn>
			</ul>
		</div>
		<div id="list-badge" class="content scaffold-list" role="main">
			<h1><g:message code="badge.list.label" args="Badge list" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<th><g:message code="badge.name.label" default="Name" /></th>

						<th><g:message code="badge.description.label" default="Description" /></th>
							
					</tr>
				</thead>
				<tbody>
				<g:each in="${badgeInstanceList}" status="i" var="badgeInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td>${fieldValue(bean: badgeInstance, field: "name")}</td>
					
						<td>${fieldValue(bean: badgeInstance, field: "description")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${badgeInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
