
<%@ page import="com.askoverflow.Question" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'question.label', default: 'Question')}" />
		<title><g:fieldValue bean="${questionInstance}" field="title"/> - Ask Over Flow</title>
	</head>
	<body>
		<a href="#show-question" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link elementId="questionButton" class="list" action="index"><g:message code="question.list.label" default="Question list" /></g:link></li>

				<li><g:link elementId="tagButton" class="list" controller="tag" action="index"><g:message code="tag.list.label" default="Tag list" /></g:link></li>

				<li><g:link elementId="badgeButton" class="list" controller="badge" action="index"><g:message code="badge.list.label" default="Badge list" /></g:link></li>

				<li><g:link elementId="userButton" class="list" controller="user" action="index"><g:message code="user.list.label" default="User list" /></g:link></li>

				<li><g:link class="create" action="create"><g:message code="question.new.label" default="Ask a question" /></g:link></li>

				<sec:ifLoggedIn>
					<li><g:link elementId="profileButton" class="list" controller="user" action="show" id="${sec.loggedInUserInfo(field: 'id')}"><sec:username/></g:link></li>
				</sec:ifLoggedIn>
			</ul>
		</div>
		<div id="show-question" class="content scaffold-show" role="main">
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

			<div id="question_${questionInstance.id}">

				<h1><span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${questionInstance}" field="title"/></span></h1>

				<table class="post">
		        <tbody>
		        <tr class="post-tr">
		            <td class="votecell">    
						<div class="vote">
							<span id="tags-label" class="property-label"><g:message code="question.vote.label" default="Vote" /></span>

		    				<span class="property-value" aria-labelledby="vote-label"><div id="result_question_${questionInstance.id}">${questionInstance.vote}</div></span>
						</div>
		            </td>
		            
					<td class="answercell">
						<div class="posttext">
							<span class="property-value" aria-labelledby="content-label"><g:fieldValue bean="${questionInstance}" field="content"/></span>
						</div>
						<table class="postinfo">
						<tbody>
						<tr class="post-tr">
							<td class="infocell">
								<span id="tags-label" class="property-label"><g:message code="question.tags.label" default="Tags" /> : </span>
								
								<g:each in="${questionInstance.tags}" var="t">
								<span class="property-value" aria-labelledby="tags-label"><g:link controller="tag" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></span>
								</g:each>

							</td>
							<td class="infocell">
								<span id="tags-label" class="property-label"><g:message code="question.author.label" default="Author" /> : </span>

								<span class="property-value" aria-labelledby="author-label"><g:link controller="user" action="show" id="${questionInstance?.author?.id}">${questionInstance?.author?.encodeAsHTML()}</g:link></span>
							</td>
							<td class="infocell">
								<span class="property-value" aria-labelledby="publicationDate-label"><g:formatDate date="${questionInstance?.publicationDate}" /></span>
							</td>
						</tr>
						</tbody>
						</table>
		    		</td>
				</tr>
				</tbody>
				</table>

				<g:form url="[resource:questionInstance, action:'delete']" method="DELETE">
					<fieldset class="buttons">
						<g:remoteLink class="upvote" action="upVote" id="${questionInstance.id}" update="[success: "result_question_${questionInstance.id}"]"><g:message code="default.button.upVote.label" default="UpVote" /></g:remoteLink>

						<g:remoteLink class="downvote" action="downVote" id="${questionInstance.id}" update="[success: "result_question_${questionInstance.id}"]"><g:message code="default.button.downVote.label" default="DownVote" /></g:remoteLink>

						<sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_MODERATOR">
							<g:remoteLink class="edit" action="edit" resource="${questionInstance}" update="[success: "question_${questionInstance.id}"]"><g:message code="default.button.edit.label" default="Edit" /></g:remoteLink>
						
							<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</sec:ifAnyGranted>
					</fieldset>
				</g:form>
			</div>

			<ol class="property-list question">

				<g:if test="${questionInstance?.answers}">
					<g:render template="/answer/view" collection="${questionInstance.answers}" var="answerInstance"/>
				</g:if>

				<div id="create-answer" class="content scaffold-create" role="main">

					<fieldset class="buttons">
						<g:remoteLink class="buttonCreate" controller="answer" action="create" params="['question.id': questionInstance?.id]" update="[success: "create-answer"]"><g:message code="question.new.answer.label" default="New response" /></g:remoteLink>
					</fieldset>

				</div>
			</ol>
		</div>
	</body>
</html>
