
<%@ page import="com.askoverflow.Question" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'question.label', default: 'Question')}" />
		<title>Ask Over Flow</title>
	</head>
	<body>
		<a href="#list-question" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link elementId="tagButton" class="list" controller="tag" action="index"><g:message code="tag.list.label" default="Tag list" /></g:link></li>

				<li><g:link elementId="badgeButton" class="list" controller="badge" action="index"><g:message code="badge.list.label" default="Badge list" /></g:link></li>
				
				<li><g:link elementId="userButton" class="list" controller="user" action="index"><g:message code="user.list.label" default="User list" /></g:link></li>

				<li><g:link class="create" action="create"><g:message code="question.new.label" default="Ask a question" /></g:link></li>

				<sec:ifLoggedIn>
					<li><g:link elementId="profileButton" class="list" controller="user" action="show" id="${sec.loggedInUserInfo(field: 'id')}"><sec:username/></g:link></li>
				</sec:ifLoggedIn>
			</ul>
		</div>
		<div id="list-question" class="content scaffold-list" role="main">
			<h1><g:message code="question.list.label" default="Question list" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>

						<th><g:message code="question.vote.label" default="Vote" /></th>

						<th><g:message code="question.title.label" default="Title" /></th>

						<th><g:message code="question.tags.label" default="Tags" /></th>

						<th><g:message code="question.author.label" default="Author" /></th>

						<th><g:message code="question.publicationDate.label" default="Publication Date" /></th>

					</tr>
				</thead>
				<tbody>
				<g:each in="${questionInstanceList}" status="i" var="questionInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td>${fieldValue(bean: questionInstance, field: "vote")}</td>

						<td><g:link action="show" id="${questionInstance.id}">${questionInstance?.truncateToString(25).encodeAsHTML()}</g:link></td>

						<td>
							<g:each in="${questionInstance.tags}" var="t">
								<span class="property-value" aria-labelledby="tags-label"><g:link controller="tag" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></span>
							</g:each>
						</td>
					
						<td>${fieldValue(bean: questionInstance, field: "author")}</td>

						<td><g:formatDate date="${questionInstance.publicationDate}" /></td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${questionInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
