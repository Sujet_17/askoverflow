<h1><g:message code="question.edit.label" default="Edit the question" /></h1>

<g:hasErrors bean="${questionInstance}">
<ul class="errors" role="alert">
    <g:eachError bean="${questionInstance}" var="error">
    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
    </g:eachError>
</ul>
</g:hasErrors>
<g:form url="[resource:questionInstance, action:'update']" method="PUT" >
    <g:hiddenField name="version" value="${questionInstance?.version}" />
    <fieldset class="form">
        <g:render template="formEdit"/>
    </fieldset>
    <fieldset class="buttons">
        <g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
    </fieldset>
</g:form>