<!DOCTYPE html>
<html>
	<head>
		<title><g:message code="error.server.title" default="Error" /> - Ask Over Flow<</title>
		<meta name="layout" content="main">
		<asset:stylesheet src="errors.css"/>
	</head>
	<body>
		<h1><g:message code="error.server.label" default="Error" /></h1>
		<ul class="errors">
			<li><g:message code="error.server.message" default="An error has occurred" /></li>
		</ul>
		<g:if env="development">
			<g:renderException exception="${exception}" />
		</g:if>
	</body>
</html>
