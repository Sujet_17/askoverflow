<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="admin.show.label" default="Admin" /> - Ask Over Flow</title>
	</head>
	<body>
		<div id="page-body" class="content scaffold-show" role="main">
			<h1><g:message code="admin.show.label" default="Admin" /></h1>

			<div id="controller-list" role="navigation">
				<ul class="controllerList">
					<g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">
						<g:if test="${c.getStaticPropertyValue('linkMe', Boolean)}">
					        <li class="controllerButtons">
					        	<g:link elementId="${c.logicalPropertyName}Button" class="styleButton" controller="${c.logicalPropertyName}"><g:message code="${c.logicalPropertyName}.controller.label" default="Controller ${c.fullName}" /></g:link>
					        </li>
					    </g:if>
					</g:each>
				</ul>
			</div>
		</div>
	</body>
</html>
