<%@ page import="com.askoverflow.Answer" %>

<h1><g:message code="answer.edit.label" default="Edit the answer" /></h1>

<g:hasErrors bean="${answerInstance}">
<ul class="errors" role="alert">
    <g:eachError bean="${answerInstance}" var="error">
    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
    </g:eachError>
</ul>
</g:hasErrors>
<g:form url="[resource:answerInstance, action:'update']" method="PUT" >
    <g:hiddenField name="version" value="${answerInstance?.version}" />
    <fieldset class="form">
        <g:render template="formEdit"/>
    </fieldset>
    <fieldset class="buttons">
        <g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
    </fieldset>
</g:form>