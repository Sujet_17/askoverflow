<%@ page import="com.askoverflow.Answer" %>


<h1><g:message code="answer.create.label" default="Create an answer" /></h1>

<g:hasErrors bean="${answerInstance}">
<ul class="errors" role="alert">
    <g:eachError bean="${answerInstance}" var="error">
    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
    </g:eachError>
</ul>
</g:hasErrors>
<g:form url="[resource:answerInstance, action:'save']" >
    <fieldset class="form">
        <g:render template="form"/>
    </fieldset>
    <fieldset class="buttons">
        <g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
    </fieldset>
</g:form>