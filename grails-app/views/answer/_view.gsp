<%@ page import="com.askoverflow.Answer" %>


<div id="answer_${answerInstance.id}">
    <table class="post">
    <tbody>
    <tr class="post-tr">
        <td class="votecell">    
            <div class="vote">
                <span id="tags-label" class="property-label"><g:message code="answer.vote.label" default="Vote" /></span>

                <span class="property-value" aria-labelledby="vote-label"><div id="result_answer_${answerInstance.id}">${answerInstance.vote}</div></span>
            </div>
        </td>
        
        <td class="answercell">
            <div class="posttext">
                <span class="property-value" aria-labelledby="content-label"><g:fieldValue bean="${answerInstance}" field="content"/></span>
            </div>
            <table class="postinfo">
            <tbody>
            <tr class="post-tr">
                <td class="infocell">
                    <span id="tags-label" class="property-label"><g:message code="answer.author.label" default="Author" /> : </span>

                    <span class="property-value" aria-labelledby="author-label"><g:link controller="user" action="show" id="${answerInstance?.author?.id}">${answerInstance?.author?.encodeAsHTML()}</g:link></span>
                </td>
                <td class="infocell">
                    <span class="property-value" aria-labelledby="publicationDate-label"><g:formatDate date="${answerInstance?.publicationDate}" /></span>
                </td>
            </tr>
            </tbody>
            </table>
        </td>
    </tr>
    </tbody>
    </table>

    <g:form url="[resource:answerInstance, controller:'answer', action:'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:remoteLink class="upvote" controller="answer" action="upVote" id="${answerInstance.id}" update="[success: "result_answer_${answerInstance.id}"]"><g:message code="default.button.upVote.label" default="UpVote" /></g:remoteLink>

            <g:remoteLink class="downvote" controller="answer" action="downVote" id="${answerInstance.id}" update="[success: "result_answer_${answerInstance.id}"]"><g:message code="default.button.downVote.label" default="DownVote" /></g:remoteLink>

            <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_MODERATOR">
                <g:remoteLink class="edit" controller="answer" action="edit" resource="${answerInstance}" update="[success: "answer_${answerInstance.id}"]"><g:message code="default.button.edit.label" default="Edit" /></g:remoteLink>

                <g:actionSubmit class="delete" controller="answer" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
            </sec:ifAnyGranted> 
        </fieldset>
    </g:form>
</div>
