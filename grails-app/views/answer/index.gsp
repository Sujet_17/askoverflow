
<%@ page import="com.askoverflow.Answer" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'answer.label', default: 'Answer')}" />
		<title><g:message code="answer.list.label" default="Answer list" /> - Ask Over Flow</title>
	</head>
	<body>
		<a href="#list-answer" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link elementId="questionButton" class="list" controller="question" action="index"><g:message code="question.list.label" default="Question list" /></g:link></li>

				<li><g:link elementId="tagButton" class="list" controller="tag" action="index"><g:message code="tag.list.label" default="Tag list" /></g:link></li>

				<li><g:link elementId="badgeButton" class="list" controller="badge" action="index"><g:message code="badge.list.label" default="Badge list" /></g:link></li>

				<li><g:link elementId="userButton" class="list" controller="user" action="index"><g:message code="user.list.label" default="User list" /></g:link></li>

				<sec:ifAllGranted roles="ROLE_ADMIN">
                    <li><g:link elementId="roleButton" class="list" controller="role" action="index"><g:message code="role.list.label" default="Role list" /></g:link></li>
                </sec:ifAllGranted>

				<sec:ifLoggedIn>
					<li><g:link elementId="profileButton" class="list" controller="user" action="show" id="${sec.loggedInUserInfo(field: 'id')}"><sec:username/></g:link></li>
				</sec:ifLoggedIn>
			</ul>
		</div>
		<div id="list-answer" class="content scaffold-list" role="main">
			<h1><g:message code="answer.list.label" default="Answer list" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>

						<th><g:message code="answer.vote.label" default="Vote" /></th>

						<th><g:message code="answer.publicationDate.label" default="Publication Date" /></th>

						<th><g:message code="answer.content.label" default="Content" /></th>

						<th><g:message code="answer.author.label" default="Author" /></th>

						<th><g:message code="answer.question.label" default="Question" /></th>

					</tr>
				</thead>
				<tbody>
				<g:each in="${answerInstanceList}" status="i" var="answerInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td>${fieldValue(bean: answerInstance, field: "vote")}</td>

						<td><g:link action="show" id="${answerInstance.id}">${answerInstance?.truncateToString(20).encodeAsHTML()}</g:link></td>
					
						<td>${fieldValue(bean: answerInstance, field: "question")}</td>
						
						<td>${fieldValue(bean: answerInstance, field: "author")}</td>

						<td><g:formatDate date="${answerInstance.publicationDate}" /></td>
										
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${answerInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
