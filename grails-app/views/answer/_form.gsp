<%@ page import="com.askoverflow.Answer" %>



<div class="fieldcontain ${hasErrors(bean: answerInstance, field: 'content', 'error')} required">
	<label for="content">
		<g:message code="answer.content.label" default="Content" />
		<span class="required-indicator">*</span>
	</label>
	<g:textArea name="content" required="" value="${answerInstance?.content}"/>

</div>

<g:hiddenField id="author" name="author.id" value="${sec.loggedInUserInfo(field: 'id')}" class="many-to-one" />

<g:hiddenField id="question" name="question.id" value="${answerInstance?.question?.id}" class="many-to-one" />
