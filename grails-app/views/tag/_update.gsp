<%@ page import="com.askoverflow.Tag" %>

<h1><g:message code="tag.edit.label" default="Edit the tag" /></h1>
            
<g:hasErrors bean="${tagInstance}">
<ul class="errors" role="alert">
    <g:eachError bean="${tagInstance}" var="error">
    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
    </g:eachError>
</ul>
</g:hasErrors>
<g:form url="[resource:tagInstance, action:'update']" method="PUT" >
    <g:hiddenField name="version" value="${tagInstance?.version}" />
    <fieldset class="form">
        <g:render template="form"/>
    </fieldset>
    <fieldset class="buttons">
        <g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
    </fieldset>
</g:form>