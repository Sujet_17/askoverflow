
<%@ page import="com.askoverflow.Tag" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}" />
		<title><g:message code="tag.show.label" default="Show the tag" /> : <g:fieldValue bean="${tagInstance}" field="description"/> - Ask Over Flow</title>
	</head>
	<body>
		<a href="#show-tag" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link elementId="questionButton" class="list" controller="question" action="index"><g:message code="question.list.label" default="Question list" /></g:link></li>

				<li><g:link elementId="tagButton" class="list" action="index"><g:message code="tag.list.label" default="Tag list" /></g:link></li>

				<li><g:link elementId="badgeButton" class="list" controller="badge" action="index"><g:message code="badge.list.label" default="Badge list" /></g:link></li>

				<li><g:link elementId="userButton" class="list" controller="user" action="index"><g:message code="user.list.label" default="User list" /></g:link></li>

				<li><g:link class="create" controller="question" action="create"><g:message code="question.new.label" default="Ask a question" /></g:link></li>

				<sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_MODERATOR">
					<li><g:link class="create" action="create"><g:message code="tag.new.label" default="New tag" /></g:link></li>
				</sec:ifAnyGranted>

				<sec:ifLoggedIn>
					<li><g:link elementId="profileButton" class="list" controller="user" action="show" id="${sec.loggedInUserInfo(field: 'id')}"><sec:username/></g:link></li>
				</sec:ifLoggedIn>
			</ul>
		</div>
		<div id="show-tag" class="content scaffold-show" role="main">
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

			<div id="tag_${tagInstance.id}">

				<h1><g:message code="tag.show.label" default="Show the tag" /></h1>

				<table class="tag">
		        <tbody>
		        <tr class="tag-tr">
		            <td class="tagcell">    
						<div class="tag">

							<span id="description-label" class="property-label"><g:message code="tag.description.label" default="Description" /></span>
						
							<span class="property-value" aria-labelledby="description-label"><div><g:fieldValue bean="${tagInstance}" field="description"/></div></span>

						</div>
		            </td>
		            
					<td class="infocell">

						<table class="tag">
						<tbody>
						<tr class="tag-tr">
							<td class="listcell">
								<div class="listhead">

									<span id="questions-count" class="property-label">${tagInstance?.questions.size()}</span>

									<span id="questions-label" class="property-label"><g:message code="tag.questions.label" default="Questions" /></span>

								</div>
							</td>
						</tr>
						<tr class="tag-tr">
							<td class="listcell">
								<table class="tag">
								<tbody>
								<g:each in="${tagInstance.questions}" var="q">
									<tr class="tag-tr">

										<td class="listvotecell"><span class="property-value" aria-labelledby="questions-label"><g:fieldValue bean="${q}" field="vote"/></span>
										</td>

										<td class="listtextcell"><span class="property-value" aria-labelledby="questions-label"><g:link controller="question" action="show" id="${q.id}">${q?.truncateToString(50).encodeAsHTML()}</g:link></span>
										</td>
									</tr>
								</g:each>
								</tbody>
								</table>
							</td>
						</tr>
						</tbody>
						</table>
					</td>
				</tr>
				</tbody>
				</table>

				<g:form url="[resource:tagInstance, action:'delete']" method="DELETE">
					<fieldset class="buttons">
						<g:remoteLink class="edit" action="edit" resource="${tagInstance}" update="[success: "tag_${tagInstance.id}"]"><g:message code="default.button.edit.label" default="Edit" /></g:remoteLink>

						<g:if test="${tagInstance.questions.size() == 0}">
							<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
						</g:if>
					</fieldset>
				</g:form>
			</div>
		</div>
	</body>
</html>
