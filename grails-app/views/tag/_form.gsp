<%@ page import="com.askoverflow.Tag" %>



<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="tag.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" maxlength="15" required="" value="${tagInstance?.description}"/>

</div>
