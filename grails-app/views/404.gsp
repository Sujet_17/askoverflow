<!DOCTYPE html>
<html>
    <head>
        <title><g:message code="error.not.found.title" default="Error" /> - Ask Over Flow</title>
        <meta name="layout" content="main">
        <asset:stylesheet src="errors.css"/>
    </head>
    <body>
        <h1><g:message code="error.not.found.label" default="Error" /></h1>
        <ul class="errors">
            <li><g:message code="error.not.found.message" default="An error has occurred" /></li>
        </ul>
    </body>
</html>
