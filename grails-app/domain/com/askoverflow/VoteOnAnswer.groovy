package com.askoverflow

import org.apache.commons.lang.builder.HashCodeBuilder

class VoteOnAnswer implements Serializable {

    private static final long serialVersionUID = 1

    User user
    Answer answer

    boolean equals(other) {
        if (!(other instanceof VoteOnAnswer)) {
            return false
        }

        other.user?.id == user?.id &&
        other.answer?.id == answer?.id
    }

    int hashCode() {
        def builder = new HashCodeBuilder()
        if (user) builder.append(user.id)
        if (answer) builder.append(answer.id)
        builder.toHashCode()
    }

    static VoteOnAnswer get(long userId, long answerId) {
        VoteOnAnswer.where {
            user == User.load(userId) &&
            answer == Answer.load(answerId)
        }.get()
    }

    static boolean exists(long userId, long answerId) {
        VoteOnAnswer.where {
            user == User.load(userId) &&
            answer == Answer.load(answerId)
        }.count() > 0
    }

    static VoteOnAnswer create(User user, Answer answer, boolean flush = false) {
        def instance = new VoteOnAnswer(user: user, answer: answer)
        instance.save(flush: flush, insert: true)
        instance
    }

    static boolean remove(User u, Answer a, boolean flush = false) {
        if (u == null || a == null) return false

        int rowCount = VoteOnAnswer.where {
            user == User.load(u.id) &&
            answer == Answer.load(a.id)
        }.deleteAll()

        if (flush) { VoteOnAnswer.withSession { it.flush() } }

        rowCount > 0
    }

    static void removeAll(User u, boolean flush = false) {
        if (u == null) return

        VoteOnAnswer.where {
            user == User.load(u.id)
        }.deleteAll()

        if (flush) { VoteOnAnswer.withSession { it.flush() } }
    }

    static void removeAll(Answer a, boolean flush = false) {
        if (a == null) return

        VoteOnAnswer.where {
            answer == Answer.load(a.id)
        }.deleteAll()

        if (flush) { VoteOnAnswer.withSession { it.flush() } }
    }

    static constraints = {
        answer validator: { Answer a, VoteOnAnswer voa ->
            if (voa.user == null) return
            boolean existing = false
            VoteOnAnswer.withNewSession {
                existing = VoteOnAnswer.exists(voa.user.id, a.id)
            }
            if (existing) {
                return 'voteOnAnswer.exists'
            }
        }
    }

    static mapping = {
        id composite: ['answer', 'user']
        version false
    }
}

