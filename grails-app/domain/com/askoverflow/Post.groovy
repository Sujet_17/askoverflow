package com.askoverflow

abstract class Post
{
    int vote = 0;
    String content;
    Date publicationDate = new Date();

    static belongsTo = [author:User];

    public void upVote()
    {
        vote++;
    }

    public void downVote()
    {
        vote--;
    }

    static mapping = {
        content type: 'text'
    }

    static constraints = {
        content blank: false
    }
}
