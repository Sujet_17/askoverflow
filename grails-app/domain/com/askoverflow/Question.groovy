package com.askoverflow

class Question extends Post
{
    String title;

    public String toString()
    {
        return title;
    }

    static hasMany = [answers:Answer, tags:Tag];

    static mapping = {
        answers sort:'vote', order:'desc'
    }

    static constraints = {
        title size: 3..100, blank: false, unique: true
    }
}
