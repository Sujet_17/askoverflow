package com.askoverflow

class Role
{
	String authority;
    String name;

    public String toString()
    {
        return name;
    }

    Set<User> getUsers()
    {
        UserRole.findAllByRole(this).collect { it.user }
    }

	static mapping = {
		cache true
	}

	static constraints = {
		authority blank: false, unique: true
        name size: 5..15, blank: false, unique: true
	}
}
