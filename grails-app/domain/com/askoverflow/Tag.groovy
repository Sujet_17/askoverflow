package com.askoverflow

class Tag
{
    String description;

    public String toString()
    {
        return description;
    }

    static hasMany = [questions:Question];
    static belongsTo = Question;

    static constraints = {
        description size: 3..15, blank: false, unique: true
    }
}
