package com.askoverflow

import org.apache.commons.lang.builder.HashCodeBuilder

class EarnedBadge implements Serializable {

    private static final long serialVersionUID = 1

    User user
    Badge badge

    boolean equals(other) {
        if (!(other instanceof EarnedBadge)) {
            return false
        }

        other.user?.id == user?.id &&
        other.badge?.id == badge?.id
    }

    int hashCode() {
        def builder = new HashCodeBuilder()
        if (user) builder.append(user.id)
        if (badge) builder.append(badge.id)
        builder.toHashCode()
    }

    static EarnedBadge get(long userId, long badgeId) {
        EarnedBadge.where {
            user == User.load(userId) &&
            badge == Badge.load(badgeId)
        }.get()
    }

    static boolean exists(long userId, long badgeId) {
        EarnedBadge.where {
            user == User.load(userId) &&
            badge == Badge.load(badgeId)
        }.count() > 0
    }

    static EarnedBadge create(User user, Badge badge, boolean flush = false) {
        def instance = new EarnedBadge(user: user, badge: badge)
        instance.save(flush: flush, insert: true)
        instance
    }

    static boolean remove(User u, Badge b, boolean flush = false) {
        if (u == null || b == null) return false

        int rowCount = EarnedBadge.where {
            user == User.load(u.id) &&
            badge == Badge.load(b.id)
        }.deleteAll()

        if (flush) { EarnedBadge.withSession { it.flush() } }

        rowCount > 0
    }

    static void removeAll(User u, boolean flush = false) {
        if (u == null) return

        EarnedBadge.where {
            user == User.load(u.id)
        }.deleteAll()

        if (flush) { EarnedBadge.withSession { it.flush() } }
    }

    static void removeAll(Badge b, boolean flush = false) {
        if (b == null) return

        EarnedBadge.where {
            badge == Badge.load(b.id)
        }.deleteAll()

        if (flush) { EarnedBadge.withSession { it.flush() } }
    }

    static constraints = {
        badge validator: { Badge b, EarnedBadge eb ->
            if (eb.user == null) return
            boolean existing = false
            EarnedBadge.withNewSession {
                existing = EarnedBadge.exists(eb.user.id, b.id)
            }
            if (existing) {
                return 'earnedBadge.exists'
            }
        }
    }

    static mapping = {
        id composite: ['badge', 'user']
        version false
    }
}

