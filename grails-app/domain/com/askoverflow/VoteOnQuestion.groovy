package com.askoverflow

import org.apache.commons.lang.builder.HashCodeBuilder

class VoteOnQuestion implements Serializable {

    private static final long serialVersionUID = 1

    User user
    Question question

    boolean equals(other) {
        if (!(other instanceof VoteOnQuestion)) {
            return false
        }

        other.user?.id == user?.id &&
        other.question?.id == question?.id
    }

    int hashCode() {
        def builder = new HashCodeBuilder()
        if (user) builder.append(user.id)
        if (question) builder.append(question.id)
        builder.toHashCode()
    }

    static VoteOnQuestion get(long userId, long questionId) {
        VoteOnQuestion.where {
            user == User.load(userId) &&
            question == Question.load(questionId)
        }.get()
    }

    static boolean exists(long userId, long questionId) {
        VoteOnQuestion.where {
            user == User.load(userId) &&
            question == Question.load(questionId)
        }.count() > 0
    }

    static VoteOnQuestion create(User user, Question question, boolean flush = false) {
        def instance = new VoteOnQuestion(user: user, question: question)
        instance.save(flush: flush, insert: true)
        instance
    }

    static boolean remove(User u, Question q, boolean flush = false) {
        if (u == null || q == null) return false

        int rowCount = VoteOnQuestion.where {
            user == User.load(u.id) &&
            question == Question.load(q.id)
        }.deleteAll()

        if (flush) { VoteOnQuestion.withSession { it.flush() } }

        rowCount > 0
    }

    static void removeAll(User u, boolean flush = false) {
        if (u == null) return

        VoteOnQuestion.where {
            user == User.load(u.id)
        }.deleteAll()

        if (flush) { VoteOnQuestion.withSession { it.flush() } }
    }

    static void removeAll(Question q, boolean flush = false) {
        if (q == null) return

        VoteOnQuestion.where {
            question == Question.load(q.id)
        }.deleteAll()

        if (flush) { VoteOnQuestion.withSession { it.flush() } }
    }

    static constraints = {
        question validator: { Question q, VoteOnQuestion voq ->
            if (voq.user == null) return
            boolean existing = false
            VoteOnQuestion.withNewSession {
                existing = VoteOnQuestion.exists(voq.user.id, q.id)
            }
            if (existing) {
                return 'voteOnQuestion.exists'
            }
        }
    }

    static mapping = {
        id composite: ['question', 'user']
        version false
    }
}

