package com.askoverflow

class Badge
{
    int switchNumber;
    String name;
    String description;

    public String toString()
    {
        return name;
    }

    static mapping = {
        description type: 'text'
    }

    static constraints = {
        switchNumber min: 0, blank: false, unique: true
        name size: 3..23, blank: false, unique: true
        description blank:false
    }
}
