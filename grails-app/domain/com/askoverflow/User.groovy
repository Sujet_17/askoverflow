package com.askoverflow

class User
{
	transient springSecurityService

	String username
	String password
	boolean enabled = true
	boolean accountExpired = false
	boolean accountLocked = false
	boolean passwordExpired = false
	String email;
    int reputation = 0;
    Date incriptionDate = new Date();

    static hasMany = [questions:Question, answers:Answer];

	static transients = ['springSecurityService']

	static constraints = {
		username size: 3..10, blank: false, unique: true
		password size: 5..500, blank: false
		email email: true, blank: false, unique:true
	}

	static mapping = {
		password column: '`password`'
		questions sort:'vote', order:'desc'
        answers sort:'vote', order:'desc'
	}

	public String toString()
    {
        return username;
    }

    public void upReputation()
    {
        reputation++;
    }

    public void downReputation()
    {
        reputation--;
    }

    public void updateReputation(int reputationToBeDeleted)
    {
        reputation -= reputationToBeDeleted;
    }

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role }
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}

    Set<Badge> getEarnedBadges() {
        EarnedBadge.findAllByUser(this).collect { it.badge }
    }
}
