package com.askoverflow

import grails.transaction.Transactional

@Transactional
class DeleteQuestionService
{
    def updateVoteAndAuthorOnQuestionDelete(Question question)
    {
        deleteVoteAssociation(question);
        updateAuthorReputationOnDelete(question);
    }
    
    private def deleteVoteAssociation(Question question)
    {
        VoteOnQuestion.removeAll(question, true);
    }

    def deleteAnswerService;

    private def updateAuthorReputationOnDelete(Question question)
    {
        question.author.updateReputation(question.vote);

        for(answer in question.answers)
        {
            deleteAnswerService.updateVoteAndAuthorOnAnswerDelete(answer);
        }
    }
}
