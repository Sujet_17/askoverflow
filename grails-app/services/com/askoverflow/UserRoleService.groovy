package com.askoverflow

import grails.transaction.Transactional

@Transactional
class UserRoleService
{
    boolean roleAssociationCanBeDeleted(Role role)
    {
        boolean canBeDeleted = true;

        // if there is only one admin left
        if (role.authority == 'ROLE_ADMIN' && UserRole.findByRole(role).count() == 1)
        {
            canBeDeleted = false;
        }

        return canBeDeleted;
    }

    boolean userCanBeDeleted(User user)
    {
        boolean canBeDeleted = true;

        Set<Role> userRoles = user.getAuthorities();
        int size = userRoles.size();
        int i = 0;
        while (i < size && canBeDeleted)
        {
            canBeDeleted = roleAssociationCanBeDeleted(userRoles[i]);
            i++;
        }

        return canBeDeleted;
    }
}
