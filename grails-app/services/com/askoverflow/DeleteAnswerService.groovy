package com.askoverflow

import grails.transaction.Transactional

@Transactional
class DeleteAnswerService
{
    def updateVoteAndAuthorOnAnswerDelete(Answer answer)
    {
        deleteVoteAssociation(answer);
        updateAuthorReputationOnDelete(answer);
    }

    private def deleteVoteAssociation(Answer answer)
    {
        VoteOnAnswer.removeAll(answer, true);
    }

    private def updateAuthorReputationOnDelete(Answer answer)
    {
        answer.author.updateReputation(answer.vote);
    }
}
