package com.askoverflow

import grails.transaction.Transactional

@Transactional
class DeleteTagService
{
    boolean tagHasNoQuestion(Tag tag)
    {
        boolean hasNoQuestion = false;

        if (tag.questions.size() == 0)
        {
            hasNoQuestion = true;
        }

        return hasNoQuestion;
    }
}
