package com.askoverflow

import grails.transaction.Transactional

@Transactional
class DownVoteService
{
    def badgeService;

    def downVoteQuestion(Question question, Long userId)
    {
        // if there is no vote for this user and this question
        if (! VoteOnQuestion.exists(userId, question.id))
        {
            downVote(question);
            User currentUser = User.findById(userId);
            VoteOnQuestion.create(currentUser, question, true);

            // badge check for the current user
            badgeService.checkForBadges(currentUser);
        }
    }

    def downVoteAnswer(Answer answer, Long userId)
    {
        // if there is no vote for this user and this answer
        if (! VoteOnAnswer.exists(userId, answer.id))
        {
            downVote(answer);
            User currentUser = User.findById(userId);
            VoteOnAnswer.create(currentUser, answer, true);

            // badge check for the current user
            badgeService.checkForBadges(currentUser);
        }
    }

    private def downVote(Post post)
    {
        post.downVote();
        post.author.downReputation();

        // badge check for the author
        badgeService.checkForBadges(post.author);
    }
}
