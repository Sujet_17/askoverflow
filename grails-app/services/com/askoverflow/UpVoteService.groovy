package com.askoverflow

import grails.transaction.Transactional

@Transactional
class UpVoteService
{
    def badgeService;
    
    def upVoteQuestion(Question question, Long userId)
    {
        // if there is no vote for this user and this question
        if (! VoteOnQuestion.exists(userId, question.id))
        {
            upVote(question);
            User currentUser = User.findById(userId);
            VoteOnQuestion.create(currentUser, question, true);

            // badge check for the current user
            badgeService.checkForBadges(currentUser);
        }
    }

    def upVoteAnswer(Answer answer, Long userId)
    {
        // if there is no vote for this user and this answer
        if (! VoteOnAnswer.exists(userId, answer.id))
        {
            upVote(answer);
            User currentUser = User.findById(userId);
            VoteOnAnswer.create(currentUser, answer, true);

            // badge check for the current user
            badgeService.checkForBadges(currentUser);
        }
    }

    private def upVote(Post post)
    {
        post.upVote();
        post.author.upReputation();

        // badge check for the author
        badgeService.checkForBadges(post.author);
    }
}
