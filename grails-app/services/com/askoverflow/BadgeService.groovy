package com.askoverflow

import grails.transaction.Transactional

@Transactional
class BadgeService
{
    def checkForBadges(User user)
    {
        // recuperation of the badge list to check
        Set<Badge> allBadges = Badge.list();
        Set<Badge> earnedBadges = user.getEarnedBadges();
        Set<Badge> badgeToCheckList = (allBadges + earnedBadges) - allBadges.intersect(earnedBadges);

        // check
        for (badgeToCheck in badgeToCheckList)
        {
            int methodToCall = badgeToCheck.switchNumber;
            switch(methodToCall)
            {
                /* --------------------------------------
                       Case pattern convention

                case 0:     // badge's description
                            if (hasDoneBadgeDescription(user))
                            {
                                affectBadgeForNumber(methodToCall, user);
                            }
                            break;

                ---------------------------------------- */

                case 0:     // ask one question
                            if (hasAskedOneQuestion(user))
                            {
                                affectBadgeForNumber(methodToCall, user);
                            }
                            break;

                case 1:     // vote 10 times
                            if (hasVotedTenTimes(user))
                            {
                                affectBadgeForNumber(methodToCall, user);
                            }
                            break;

                case 2:     // became Yoda's padawan with at least 10 in reputation
                            if (hasBecomePadawanInReputation(user))
                            {
                                affectBadgeForNumber(methodToCall, user);
                            }
                            break;

                case 3:     // became Jedi master with at least 250 in reputation
                            if (hasBecomeJediInReputation(user))
                            {
                                affectBadgeForNumber(methodToCall, user);
                            }
                            break;

                case 4:     // became knowledge owner with at least 20 answers
                            if (hasBecomeKnowledgeOwner(user))
                            {
                                affectBadgeForNumber(methodToCall, user);
                            }
                            break;

                case 5:     // became apprentice with at least -10 in reputation
                            if (hasBecomeApprentice(user))
                            {
                                affectBadgeForNumber(methodToCall, user);
                            }
                            break;

                case 6:     // became Sith emperor with at least -100 in reputation
                            if (hasBecomeSithEmperor(user))
                            {
                                affectBadgeForNumber(methodToCall, user);
                            }
                            break;

                default:    // default
                            break;
            }
                        
        }
    }

    /* ------------------------------------------------
               Check methode pattern convention

    // 0
    private boolean hasDoneBadgeDescription(User user)
    {
        boolean valid = false;

        if ()
        {
            valid = true;
        }

        return valid;
    }

    ------------------------------------------------- */

    // 0
    private boolean hasAskedOneQuestion(User user)
    {
        boolean valid = false;

        if (user.questions.size() == 1)
        {
            valid = true;
        }

        return valid;
    }

    // 1
    private boolean hasVotedTenTimes(User user)
    {
        boolean valid = false;

        int nbVoteOnAnswer = VoteOnAnswer.findAllByUser(user).collect { it.answers }.size();
        int nbVoteOnQuestion = VoteOnQuestion.findAllByUser(user).collect { it.question }.size();

        if (nbVoteOnAnswer + nbVoteOnQuestion >= 10)
        {
            valid = true;
        }

        return valid;
    }

    // 2
    private boolean hasBecomePadawanInReputation(User user)
    {
        boolean valid = false;

        if (user.reputation >= 10)
        {
            valid = true;
        }

        return valid;
    }

    // 3
    private boolean hasBecomeJediInReputation(User user)
    {
        boolean valid = false;

        if (user.reputation >= 250)
        {
            valid = true;
        }

        return valid;
    }

    // 4
    private boolean hasBecomeKnowledgeOwner(User user)
    {
        boolean valid = false;

        if (user.answers.size() >= 20)
        {
            valid = true;
        }

        return valid;
    }

    // 5
    private boolean hasBecomeApprentice(User user)
    {
        boolean valid = false;

        if (user.reputation <= -10)
        {
            valid = true;
        }

        return valid;
    }

    // 6
    private boolean hasBecomeSithEmperor(User user)
    {
        boolean valid = false;

        if (user.reputation <= -100)
        {
            valid = true;
        }

        return valid;
    }


    private def affectBadgeForNumber(int badgeSwitchNumber, User user)
    {
        Badge badgeToAffect = Badge.findBySwitchNumber(badgeSwitchNumber);

        if (! EarnedBadge.exists(user.id, badgeToAffect.id))
        {
            EarnedBadge.create(user, badgeToAffect, true);
        }
    }
}
