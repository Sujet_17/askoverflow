package com.askoverflow

import grails.transaction.Transactional

@Transactional
class DeleteUserService
{
    def updateUserContentOnDelete(User user)
    {
        deleteRoleAssociation(user);
        deleteVoteAssociation(user);
        deleteBadgeAssociation(user);
        delegateAllPost(user);        
    }

    def badgeService;

    private def delegateAllPost(User user)
    {
        def unknownUser = User.findByUsername('Unknown');

        // quetion delegation
        Set<Question> questionsList = user.questions;
        for (question in questionsList)
        {
            question.author = unknownUser;
            unknownUser.addToQuestions(question);
            unknownUser.reputation += question.vote;
            question.save(flush:true);
            unknownUser.save(flush:true);
        }
        user.questions.clear();
        user.save(flush:true);

        // answer delegation
        Set<Answer> answerList = user.answers;
        for (answer in answerList)
        {
            answer.author = unknownUser;
            unknownUser.addToAnswers(answer);
            unknownUser.reputation += answer.vote;
            answer.save(flush:true);
            unknownUser.save(flush:true);
        }
        user.answers.clear();
        user.save(flush:true);

        // badge check for unknownUser
        badgeService.checkForBadges(unknownUser);

    }

    boolean userIsUnknownUser(User user)
    {
        boolean isUnknownUser = false;

        if (user.username == 'Unknwon')
        {
            isUnknownUser = true;
        }

        return isUnknownUser;
    }

    private def deleteRoleAssociation(User user)
    {
        UserRole.removeAll(user, true);
    }

    private def deleteVoteAssociation(User user)
    {
        VoteOnAnswer.removeAll(user, true);
        VoteOnQuestion.removeAll(user, true);
    }

    private def deleteBadgeAssociation(User user)
    {
        EarnedBadge.removeAll(user, true);
    }
}
