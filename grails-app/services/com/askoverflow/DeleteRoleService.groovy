package com.askoverflow

import grails.transaction.Transactional

@Transactional
class DeleteRoleService
{
    def deleteUserAssociation(Role role)
    {
        UserRole.removeAll(role, true);
    }
}
